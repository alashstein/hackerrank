# for i = 2:n,
#     for (k = i; k > 1 and a[k] < a[k-1]; k--)
#         swap a[k,k-1]
#     → invariant: a[1..i] is sorted
# end

from BubbleSort import printcolor

a = [6, 4, 5, 7, 2, 1, 3]
[6, 4, 5, 7, 0, 2, 1, 8,3,9 ]
n = len(a)

def swap(x, y):
  # a[x] , a[y] = a[y] , a[x]
  temp = a[x]
  a[x] = a[y]
  a[y] = temp
  return x , y

def insertion(n):
  step = 0
  for i in range(1, n): # 2 diganti satu karena array di python dimulai dari 0
    k = i
    while ( k > 0 and a[k] < a[k-1]): # k > 1 diganti menjadi k > 0 karena array di python dimulai dari 0
      swap(k, k-1)
      printcolor(step, a, k, k - 1, True)
      k -= 1
      step += 1
    print("---")
  print("")
  printcolor("#", a, k, k - 1, True)

def insertion1(a, n):
  print(a)
  step = 0
  for i in range(1, n): # 2 diganti satu karena array di python dimulai dari 0
    k = i
    while ( k > 0 and a[k] < a[k-1]): # k > 1 diganti menjadi k > 0 karena array di python dimulai dari 0
      swap(k, k-1)
      printcolor(step, a, k, k - 1, True)
      k -= 1
      step += 1
    print("---")
  print("")
  printcolor("#", a, k, k - 1, True)


insertion(n)