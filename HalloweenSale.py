p = 20 
d = 3 
m = 6 
s = 80

def howManyGames(p, d, m, s):
    games = 0
    while s >= 0:
        s -= p
        p = max(p - d, m)
        games += 1
    print(games - 1)
howManyGames(p, d, m, s)